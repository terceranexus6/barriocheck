import folium
import json

map = folium.Map(location=[40.3868601, -3.6591803], default_zoom_start=180)

coordsList = []
print("Started Reading JSON file which contains multiple JSON document")
with open('ejemplo/coords.txt') as f:
    for jsonObj in f:
        coordsDict = json.loads(jsonObj)
        coordsList.append(coordsDict)

print("Printing each JSON Decoded Object")
for coord in coordsList:
    print(coord["id"], coord["coor1"], coord["coor2"], coord["mensaje"])
    folium.Marker(
        location=[float(coord["coor1"]), float(coord["coor2"])],
        popup=coord["mensaje"],
        tooltip = "Información del aviso"
    ).add_to(map)


print("Creating map...")
map.save('mapa.html')
 
  
